<!DOCTYPE html>
<?php
require('model/stand.php');
$req = getStand();
?>
<html lang="fr-FR">
    <head>
        <!-- Meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Font family google -->
        <link href="https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Calligraffitti&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Romanesco&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet">

        <!-- style.css -->
        <link href="ressources/css/style.css" rel="stylesheet">

        <title>JoJo's Bizarre Adventure</title>
    </head>
    <body id="bodyStand">
        <div class="container" id="main">
            <?php
                require('layout/navbar.php')
            ?>
            <div class="stand">
                <div class="row col-sm">
                    <h1>Les Stands :</h1>
                </div>

                <div class="standPresentation">
                    <?php
                    while ($data = $req->fetch())
                    {
                        ?>
                        <div class="row col-sm">
                            <h4><?php echo $data['nom']; ?></h4>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <div class="row">
                                    <h2>Résumé :</h2>
                                </div>
                                <div class="row">
                                    <p><?php echo $data['resume']; ?><br>
                                        <?php echo $data['apparence']; ?>
                                    </p>
                                </div>
                                <div class="row">
                                    <h2>Ses Principaux Pouvoirs :</h2>
                                </div>
                                <div class="row">
                                    <div class="row col-sm">
                                        <h5><?php echo $data['power1']; ?></h5>
                                    </div>
                                    <div class="row col-sm">
                                        <h5><?php echo $data['power2']; ?></h5>
                                    </div>
                                    <div class="row col-sm">
                                        <h5><?php echo $data['power3']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <img src="<?php echo $data['image']; ?>" width="500" height="800">
                            </div>
                        </div>
                        <?php
                    }
                    $req->closeCursor();
                    ?>
                </div>
            </div>
        </div>
    </body>

    <!-- Bootstrap JS/JQUERY -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>