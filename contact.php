<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <!-- Meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Font family google -->
        <link href="https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Romanesco&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Calligraffitti&display=swap" rel="stylesheet">

        <!-- style.css -->
        <link href="ressources/css/style.css" rel="stylesheet">

        <title>JoJo's Bizarre Adventure</title>
    </head>
    <body id="bodyContact">
        <div class="container" id="main"> <!-- main -->
            <?php
                require('layout/navbar.php')
            ?>
            <div class="contact">
                <div class="row col-sm">
                    <h1>Page Contact</h1>
                </div>
                <div class="row col-sm">
                    <h2>Technos utilisé pour ce site :</h2>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <h4>HTML5</h4>
                    </div>
                    <div class="col-sm">
                        <h4>CSS3</h4>
                    </div>
                    <div class="col-sm">
                        <h4>Bootstrap</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <h4>MySQL</h4>
                    </div>
                    <div class="col-sm">
                        <h4>JavaScript</h4>
                    </div>
                </div>
                <div class="row col-sm">
                    <h2>Site réalisé par :</h2>
                </div>
                <div class="row col-sm">
                    <h4>Marvin COUSIN</h4>
                </div>
                <div class="row col-sm">
                    <h2>CV :</h2>
                </div>
                <div class="row col-sm">
                    <a href="ressources/files/CV%20Marvin%20COUSIN.pdf" download>
                        <h4 style="color: black">Cliquez ici pour avoir mon CV</h4>
                    </a>
                </div>
                <div class="row col-sm">
                    <h2>Profil Linkedin</h2>
                </div>
                <div class="row col-sm">
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Cliquez ICI</button>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Marvin COUSIN : </h4>
                                </div>
                                <div class="modal-body">
                                    <a href="https://www.linkedin.com/in/marvin-cousin-b3b865138/">
                                        <h4>Profil Linkedin</h4>
                                    </a>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- END main -->
    </body>
    <!-- Bootstrap JS/JQUERY -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>