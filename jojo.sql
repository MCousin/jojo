-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 23 août 2019 à 23:26
-- Version du serveur :  5.7.26
-- Version de PHP :  7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `jojo`
--

-- --------------------------------------------------------

--
-- Structure de la table `stand`
--

DROP TABLE IF EXISTS `stand`;
CREATE TABLE IF NOT EXISTS `stand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `apparence` varchar(255) NOT NULL,
  `power1` varchar(255) NOT NULL,
  `power2` varchar(255) NOT NULL,
  `power3` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `stand`
--

INSERT INTO `stand` (`id`, `nom`, `resume`, `apparence`, `power1`, `power2`, `power3`, `image`) VALUES
(1, 'Star Platinum', 'Star Platinum est le stand de Jotaro Kujo.\r\nCe stand est réputé pour être l\'un des plus puissants de toute la saga. ', 'Star Platinum est un stand qui s\'apparente plus ou moins à un homme, très bien bâti à l\'exception des oreilles en forme de tubes. Il a les cheveux dressés, une courte écharpe, de longs gants noirs cloutés sur le dos des mains.', 'Force', 'Protection de son hôte', 'Star Finger', 'ressources/images/star_platinum.jpg'),
(2, 'Magician\'s Red', 'Magician\'s Red est le stand de Mohammed Abdul', 'Magician\'s Red a une apparence humanoïde, sa tête s\'apparente à celle d\'un oiseau.\r\nEtant torse-nu, il porte des brassards, et un pantalon fait de plumes rouges. ', 'Manipulation des Flammes', 'Crossfire Hurricane', 'Red Bind', 'ressources/images/Magician_Red.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
