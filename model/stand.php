<?php

function getStand()
{
    try
    {
        // Connexion à la bdd
        $db = new PDO('mysql:host=localhost;dbname=jojo;charset=utf8', 'root', '');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (Exception $e)
    {
        die('Erreur : ' . $e->getMessage());
    }
    // Récupération des données
    $req = $db->prepare("SELECT id, nom, resume, apparence, power1, power2, power3, image FROM stand ORDER BY id");
    $req->execute();

    return $req;
}

