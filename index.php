<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <!-- Meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Font family google -->
        <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet">

        <!-- style.css -->
        <link href="ressources/css/style.css" rel="stylesheet">

        <title>JoJo's Bizarre Adventure</title>
    </head>
    <body id="bodyAccueil">
        <div class="container" id="main"><!-- main -->
            <?php
                require('layout/navbar.php')
            ?>
            <div class="intro">
                <h1><img src="ressources/images/titre-accueil.svg"></h1>
                    <p>
                        JoJo's Bizarre Adventure (ジョジョの奇妙な冒険, JoJo no kimyō na bōken) est un manga de Hirohiko Araki.
                        Il a été prépublié entre 1986 et 2004 dans l'hebdomadaire Weekly Shōnen Jump,
                        et est prépublié depuis 2005 dans le mensuel Ultra Jump à la suite du changement de cible éditoriale opéré dans la septième partie du manga.
                    </p>
                    <p>
                        La version française est d'abord sortie aux éditions "J'ai lu" en 46 tomes, mais la publication fut abandonnée en 2006.
                        Tonkam a alors repris l'édition à partir de la cinquième partie, Golden Wind. L'éditeur réédite par la suite les premières parties,
                        en commençant par la troisième.
                        Le manga est adapté en une série d'OAV de treize épisodes, qui retrace une partie de la troisième partie du manga, produite par Studio A.P.P.P. et éditée en France par Déclic Images.
                    </p>
                    <p>
                        Une adaptation en série télévisée d'animation de 26 épisodes produite par David Production est diffusée entre octobre 2012 et mars 2013 et adapte les deux premières parties du manga.
                        L'adaptation de la troisième partie est diffusée entre avril 2014 et juin 2015. L'adaptation de la quatrième partie est diffusée entre avril et décembre 2016.
                        L'adaptation de la cinquième partie est diffusée entre octobre 2018 et juin 2019.
                    </p>
            </div>
            <div class="carousel"><!-- carousel -->
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="ressources/images/partie1.png" alt="Image 1">
                            <div class="carousel-caption d-none d-md-block">
                                <h3>Partie 1 : Phamtom Blood</h3>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="ressources/images/partie2.jpg" alt="Image 2">
                            <div class="carousel-caption d-none d-md-block">
                                <h3>Partie 2 : Battle Tendency</h3>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="ressources/images/partie3.jpeg" alt="Image 3">
                            <div class="carousel-caption d-none d-md-block">
                                <h3>Partie 3 : Stardust Crusaders</h3>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="ressources/images/partie4.png" alt="Image 4">
                            <div class="carousel-caption d-none d-md-block">
                                <h3>Partie 4 : Diamond is Unbreakable</h3>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="ressources/images/partie5.jpg" alt="Image 5">
                            <div class="carousel-caption d-none d-md-block">
                                <h3>Partie 5 : Golden Wind</h3>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true" ></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div><!-- END carousel -->
        </div><!-- END main -->
    </body>

    <!-- Bootstrap JS/JQUERY -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>