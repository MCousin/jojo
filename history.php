<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <!-- Meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Font family google -->
        <link href="https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Romanesco&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Calligraffitti&display=swap" rel="stylesheet">

        <!-- style.css -->
        <link href="ressources/css/style.css" rel="stylesheet">

        <title>JoJo's Bizarre Adventure</title>
    </head>
    <body id="bodyHistory">
        <div class="container" id="main"><!-- main -->
            <?php
                require('layout/navbar.php')
            ?>
            <div class="part1"><!-- part1 -->
                <div class="row col-sm">
                    <h1>Partie 1 : Phamtom Blood</h1>
                </div>
                <div class="row col-sm">
                    <h2>Synopsis :</h2>
                </div>
                <div class="row col-sm">
                    <p>Nous voici à la fin du XIXe siècle en pleine Angleterre victorienne.
                        Lord Joestar voit arriver dans sa maison le jeune Dio Brando, le fils de l'homme qu'il croit lui avoir sauvé la vie.
                        Reconnaissant, il l'adopte après la mort de son père, mais le jeune homme s'avère ambitieux et n'a pour seul but que de s'emparer de la fortune des Joestar.
                        Tous deux désormais à l'université, il semblerait presque être des frères unis.
                        Malgré les bonnes intentions de Dio, le doute persiste, jusqu'à ce que Jonathan découvre que Dio a commis un parricide et tente désormais d'empoisonner aussi son père.
                        Et quand un mystérieux ancien masque de pierre aztèque renfermant d'étranges pouvoirs apparaît, leur rivalité latente prend des proportions dramatiques et surnaturelles.
                        Jonathan révèle au grand jour les plans machiavéliques de son frère adoptif. Acculé et sur le point de se retrouver livré à la police, Dio utilise cet étrange artefact.
                        Utilisant le sang de Lord Joestar, il abandonne son humanité et devient un monstre aux pouvoirs surhumains, craignant la lumière du jour et s'abreuvant de sang humain.
                        Il tue les policiers présents avant de s'attaquer au héros encore sous le choc de la mort de son père.
                        Seul l'apprentissage d'une ancienne technique martiale tibétaine, l'Onde, lui permet de vaincre celui qui change irrémédiablement sa destinée.
                        Aidé de son nouveau compagnon Speedwagon et de son maître Zeppeli,
                        il se prépare enfin à défaire les légions de zombies envoyés par son ancien frère adoptif pour l'exterminer.
                    </p>
                </div>
                <div class="row col-sm">
                    <h2>Heros et Antagoniste :</h2>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Jonathan Joestar</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/jonathan_joestar.png" width="400" height="500">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Dio Brando</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/dio_brando.png" width="400" height="500">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END part1 -->
            <hr>
            <div class="part2"><!-- part2 -->
                <div class="row col-sm">
                    <h1>Partie 2 : Battle Tendency</h1>
                </div>
                <div class="row col-sm">
                    <h2>Synopsis :</h2>
                </div>
                <div class="row col-sm">
                    <p>Les années 1930, la grande dépression. Bien des temps ont passé depuis la dramatique conclusion de la confrontation entre Dio Brando et Jonathan Joestar.
                        Joseph, petit-fils de ce dernier, s'avère être un surdoué qui maîtrise l'Onde depuis son plus jeune âge. Tête de mule et dur à cuire,
                        il vient d'arriver à New York et apprend que Speedwagon aurait été tué... Parti à sa recherche au Mexique, il se retrouvera à combattre une forme de vie ultime réveillée par les nazis, Santana.
                        Quand il apprend que trois autres de ses congénères sont sur le point de se réveiller à Rome, le voilà parti pour combattre ces êtres d'un autre temps.
                        Il s'y fera un ami précieux et fera face à de bouleversantes révélations familiales au long de cette rude bataille...
                        Joseph sera-t-il assez fort pour défaire ces trois menaces ? Quels drames et révélations l'attendent en Europe ?
                        Cette partie comporte les prémices de Stardust Crusaders. Notre héros voyage à travers le monde et y découvre les spécificités locales (culinaires en particulier).
                        La série s'avère ce coup-ci plus proche d'un manga d'arts martiaux comme Hokuto no Ken et les opposants se battent de manière plus loyale que le fourbe Dio.
                        Le graphisme y est beaucoup plus travaillé et on commence à y trouver les fameux passages bizarres et déconcertants,
                        emblématiques de la série (notamment un combat de pâtes ou une course de chevaux-vampires).
                        C'est aussi dans cette partie que commencent à apparaître clairement les références à la musique occidentale, principalement dans les noms des personnages (Santana, Esidisi, Wamuu).
                        Cette partie possède une pléthore d'éléments nanar tout à fait assumés et mis à dessein : des vampires, des nazis, du kung-fu, un Joseph qui cabotine à longueur de temps, de l'exotisme,
                        des scènes complètement loufoques aux limites du ridicule (Joseph en travesti infiltre un camp nazi)... Toutefois, ce côté étant tout à fait voulu,
                        cela ne fait que renforcer l'effet de surenchère et on entre de plain-pied dans cet aspect "bizarre" de JoJo's Bizarre Adventure qui le différencie des autres mangas.
                    </p>
                </div>
                <div class="row col-sm">
                    <h2>Heros et Antagoniste :</h2>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Joseph Joestar</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/joseph_joestar.webp" width="400" height="500">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4 style="margin-right: 20px">Kars</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/kars.webp" width="400" height="500">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- part2 -->
            <hr>
            <div class="part3"><!-- part3 -->
                <div class="row col-sm">
                    <h1>Partie 3 : Stardust Crusaders</h1>
                </div>
                <div class="row col-sm">
                    <h2>Synopsis :</h2>
                </div>
                <div class="row col-sm">
                    <p>1989, Tokyo, Japon. Voici la destinée de Jotaro, petit-fils de Joseph et fils de Holy. Lycéen voyou et caractériel,
                        il est enfermé en prison lorsque son grand-père, appelé en urgence, arrive. Jotaro semble possédé par un esprit et refuse de sortir de sa cellule.
                        Il ignore qu'en fait il possède un Stand, un double psychique représenté sous la forme d'un corps astral aux pouvoirs incroyables.
                        Une fois que son grand-père l'a mis au courant de la nature de son nouveau pouvoir, il lui apprend une bien terrible nouvelle.
                        Dio, ennemi héréditaire de leur lignée, s'est réveillé d'entre les morts. De plus, le réveil de Dio a déclenché l'apparition des Stands de la famille Joestar.
                        Seul problème, une terrible fièvre chez Holy, mère de Jotaro et fille de Joseph, se développe également et menace sa vie.
                        Son esprit pacifique rejette le Stand et si Dio n'est pas annihilé d'ici cinquante jours, elle en perdra la vie !
                        Parti avec deux compagnons eux aussi possesseurs d'un Stand, Joseph et Jotaro partent pour l'Égypte afin de réaliser le but de leur destinée :
                        détruire le démon une bonne fois pour toutes et sauver celle qu'ils aiment. Mais Dio est bien décidé à les tuer et envoie plusieurs assassins dotés d'un Stand afin de stopper leur avancée.
                        Jotaro et ses amis arriveront-ils à en finir avec le terrible Dio qui semble lui aussi doté d'un Stand particulièrement redoutable ? Arriveront-ils à découvrir son terrible secret et à sauver Holy ?
                        Partie sans aucun doute la plus populaire et la plus connue du manga à travers ses diverses adaptations, elle coïncide avec l'arrivée des Stands,
                        l'arrivée à maturité de son créateur et un certain classicisme dans son déroulement qui se rapproche plus des canons du shōnen habituel.
                        Une quête clairement présentée dès le début, un ennemi final bien connu et aussi une équipe de héros au caractère complémentaires et bien affirmés.
                        Aussi, un voyage à travers l'Asie et le Moyen-Orient parsemé de petites anecdotes amusantes.
                        Mais ces prémices classiques au contraire de desservir le récit permettront à Hirohiko Araki,
                        l'auteur, de s'appuyer sur des bases solides qui lui permettront de donner libre cours à son imagination et parfaire sa grande invention : les Stands.
                    </p>
                </div>
                <div class="row col-sm">
                    <h2>Heros et Antagoniste :</h2>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Jotaro Kujo</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/jotaro_kujo.jpg" width="400" height="500">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Dio Brando</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/dio_brando.png" width="400" height="500">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END part3 -->
            <hr>
            <div class="part4"><!-- part4 -->
                <div class="row col-sm">
                    <h1>Partie 4 : Diamond is Unbreakable</h1>
                </div>
                <div class="row col-sm">
                    <h2>Synopsis :</h2>
                </div>
                <div class="row col-sm">
                    <p>1999, Morio, Japon. C'est la rentrée scolaire au Japon et Jotaro vient rencontrer un certain Josuke Higashikata.
                        Jeune lycéen, il s'avère être à son grand étonnement l'oncle de notre ancien héros (fruit d'une relation adultère de Joseph avec une Japonaise).
                        Il vient aussi le mettre en garde contre un mystérieux manieur de Stand qui vient menacer la vie paisible de sa petite ville.
                        Troublé, Josuke se voit obliger d'accepter sa destinée lorsque le danger vient frapper à sa porte :
                        protéger sa ville et ceux qu'il aime des malveillants manieurs de Stand qui commencent à apparaître...
                        Est-ce que Jôsuke trouvera le courage de protéger sa ville de leurs assauts ?
                        D'où vient cette étrange recrudescence de manieurs de Stands ? Et surtout, comment combattre une menace tapie dans l'ombre depuis des années se révélant au grand jour ?
                    </p>
                </div>
                <div class="row col-sm">
                    <h2>Heros et Antagoniste :</h2>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Josuke Higashikata</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/josuke.webp" width="400" height="500">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Yoshikage Kira</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/yoshikage_kira.webp" width="400" height="500">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END part4 -->
            <hr>
            <div class="part5"><!-- part5 -->
                <div class="row col-sm">
                    <h1>Partie 5 : Golden Wind</h1>
                </div>
                <div class="row col-sm">
                    <h2>Synopsis :</h2>
                </div>
                <div class="row col-sm">
                    <p>2001, Neapolis, Italie. Le jeune Giorno Giovanna décide de grimper les échelons de la mafia,
                        afin qu'une fois en haut il puisse arrêter le trafic de drogue et les violences qui empoisonnent sa ville.
                        Ayant rencontré dans sa jeunesse un gangster juste qui protégeait son quartier, il a décidé à son tour d'agir de la même façon.
                        Jeune, battu par son beau-père, il avait perdu foi en lui-même jusqu'à ce que ce gangster lui apprenne la confiance en l'autre et devienne son protecteur.
                        Depuis il souhaite devenir un grand mafieux afin de protéger son quartier et les plus faibles comme son modèle car la police s'avère être incapable de s'en assurer.
                        Nous suivrons donc les péripéties de GioGio et ses futurs compagnons, des gangsters à l'ancienne, respectueux du code d'honneur...
                        Quel est le lien étonnant de ce jeune homme à la famille Joestar ? Giorno atteindra-t-il son rêve ? Et quel terrible mystère cache l'énigmatique parrain pour lequel il travaille désormais ?
                    </p>
                </div>
                <div class="row col-sm">
                    <h2>Heros et Antagoniste :</h2>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Giorno Giovanna</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/giorno_giovanna.webp" width="400" height="500">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-sm">
                                <h4>Diavolo</h4>
                            </div>
                            <div class="col-sm portrait">
                                <img src="ressources/images/diavolo.jpg" width="400" height="500">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END part5 -->
            <hr>
        </div><!-- END main -->
    </body>

    <!-- Bootstrap JS/JQUERY -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>